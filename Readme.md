# Browser Stress Test

Very simple Browser stress test with server- and client side.

## Server

nodejs server using express.

To get it up and running, install nodejs on your computer,
then enter this directory and run
```
npm install
npm start
```

after that, the server should run on port 3000

## Client

Simply open a browser and head to the url of the server, e.g. [localhost:3000](http://localhost:3000)

## Version History
1.0 Initial Version 24.2.2021