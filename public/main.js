"use strict";

function getCounter() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            document.getElementById("counter").innerHTML = xmlHttp.responseText;
            setTimeout(getCounter, 10)
        }
    }
    xmlHttp.open( "GET", "/count", true ); // false for synchronous request
    xmlHttp.send( null );
}

function startup() {
    console.log("Application Started");
    document.getElementById("counter").innerHTML="Application Started";
    // start 10 counter instances
    for (let i=0; i<10; i++) {
        getCounter();
    }
}

window.onload = startup;