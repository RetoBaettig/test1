"use strict"
const express = require('express');
require('log-timestamp');

const app = express();

//let PORT = process.env.PORT;
let PORT = process.env.PORT || 3000;
let counter = 0;

app.use(express.static('public'));

app.get('/count', (req, res) => {
    counter++;
    if (counter % 1000 == 0) {
        console.log(counter+" requests served");
    }
    res.send({success: true, data: counter});
});

app.listen(PORT, () => {
        console.log(`Echo app listening on port ${PORT}!`);
        console.log('Counting on route /count');
    }
);

